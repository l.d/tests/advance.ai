package com.dotronglong.tests.advanceai.two;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class SimpleGraphFinder implements GraphFindable {
    public SimpleGraphFinder() {
    }

    public LinkedList<DirectedAcyclicGraph.Node> findPathWithMaxWeight(DirectedAcyclicGraph graph) {
        return new LinkedList<>();
    }

    @Override
    public LinkedList<DirectedAcyclicGraph.Node> findPathWithMaxWeightFromOrigin(DirectedAcyclicGraph graph, DirectedAcyclicGraph.Node origin) {
        LinkedList<String> stack = new LinkedList<>();
        Map<String, Integer> distances = new HashMap<>();
        Map<String, Boolean> visitedNodes = new HashMap<>();

        graph.getNodes().forEach((name, node) -> {
            visitedNodes.put(name, false);
            distances.put(name, Integer.MAX_VALUE);
        });
        distances.put(origin.getName(), 0);
        graph.getNodes().forEach((name, node) -> {
            if (!visitedNodes.get(name)) {
                sortTopological(graph, name, visitedNodes, stack);
            }
        });

        String current;
        while(!stack.isEmpty()) {
            current = stack.pop();
            if (!distances.get(current).equals(Integer.MAX_VALUE)) {
                String finalCurrent = current;
                graph.getNodes().get(current).getNeighbors()
                        .forEach(node -> {
                            if (!node.getName().equals(origin.getName())
                                && (!distances.containsKey(node.getName())
                                    || distances.get(node.getName()).equals(Integer.MAX_VALUE)
                                    || distances.get(node.getName()) < distances.get(finalCurrent) + node.getWeight())) {
                                distances.put(node.getName(), distances.get(finalCurrent) + node.getWeight());
                            }
                        });
            }
        }

        LinkedList<DirectedAcyclicGraph.Node> result = new LinkedList<>();
        distances.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .forEach(entry -> result.addLast(graph.getNodes().get(entry.getKey())));

        return result;
    }

    private void sortTopological(DirectedAcyclicGraph graph,
                                 String nodeName, Map<String, Boolean> visitedNodes, LinkedList<String> stack) {
        visitedNodes.put(nodeName, true);
        graph.getNodes().forEach((name, node) -> {
            if (!visitedNodes.get(name)) {
                sortTopological(graph, name, visitedNodes, stack);
            }
        });

        stack.push(nodeName);
    }
}
