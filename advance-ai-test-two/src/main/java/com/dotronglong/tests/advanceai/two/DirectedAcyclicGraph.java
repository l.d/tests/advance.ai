package com.dotronglong.tests.advanceai.two;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DirectedAcyclicGraph {
    Map<String, Node> nodes;

    public DirectedAcyclicGraph() {
        nodes = new HashMap<>();
    }

    public DirectedAcyclicGraph(Map<String, Node> nodes) {
        this.nodes = nodes;
    }

    public void addEdge(Node node, Node neighbor) {
        if (!nodes.containsKey(node.getName())) {
            nodes.put(node.getName(), node);
        }
        if (!nodes.containsKey(neighbor.getName())) {
            nodes.put(neighbor.getName(), neighbor);
        }
        nodes.get(node.getName()).getNeighbors().add(neighbor);
    }

    public Map<String, Node> getNodes() {
        return nodes;
    }

    public void setNodes(Map<String, Node> nodes) {
        this.nodes = nodes;
    }

    public static class Node {
        private String name;
        private Integer weight;
        private List<Node> neighbors;

        public Node(String name, Integer weight) {
            this.name = name;
            this.weight = weight;
            this.neighbors = new ArrayList<>();
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getWeight() {
            return weight;
        }

        public void setWeight(Integer weight) {
            this.weight = weight;
        }

        public List<Node> getNeighbors() {
            return neighbors;
        }

        public void setNeighbors(List<Node> neighbors) {
            this.neighbors = neighbors;
        }
    }
}
