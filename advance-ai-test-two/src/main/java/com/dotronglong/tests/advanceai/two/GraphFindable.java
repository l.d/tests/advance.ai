package com.dotronglong.tests.advanceai.two;

import java.util.LinkedList;

public interface GraphFindable {
    LinkedList<DirectedAcyclicGraph.Node> findPathWithMaxWeight(DirectedAcyclicGraph graph);
    LinkedList<DirectedAcyclicGraph.Node> findPathWithMaxWeightFromOrigin(DirectedAcyclicGraph graph, DirectedAcyclicGraph.Node origin);
}
