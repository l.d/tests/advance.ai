package com.dotronglong.tests.advanceai.two;

import org.junit.Test;

import java.util.LinkedList;

import static org.junit.Assert.assertEquals;

public class SimpleGraphFinderTest {

    @Test
    public void testFindPathWithMaxWeight() {
        GraphFindable findable = new SimpleGraphFinder();
        LinkedList<DirectedAcyclicGraph.Node> nodes = findable.findPathWithMaxWeight(new DirectedAcyclicGraph());
        assertEquals(0, nodes.size());
    }

    @Test
    public void testFindPathWithMaxWeightFromOrigin_CaseOne() {
        GraphFindable findable = new SimpleGraphFinder();
        DirectedAcyclicGraph graph = new DirectedAcyclicGraph();
        DirectedAcyclicGraph.Node nodeA = new DirectedAcyclicGraph.Node("A", 1);
        DirectedAcyclicGraph.Node nodeB = new DirectedAcyclicGraph.Node("B", 2);
        DirectedAcyclicGraph.Node nodeC = new DirectedAcyclicGraph.Node("C", 2);

        graph.addEdge(nodeA, nodeB);
        graph.addEdge(nodeA, nodeC);
        graph.addEdge(nodeB, nodeC);

        LinkedList<DirectedAcyclicGraph.Node> nodes = findable.findPathWithMaxWeightFromOrigin(graph, nodeA);
        assertEquals(3, nodes.size());
        assertEquals("A", nodes.get(0).getName());
        assertEquals("B", nodes.get(1).getName());
        assertEquals("C", nodes.get(2).getName());
        nodes.forEach(node -> System.out.print(node.getName() + " -> "));
        System.out.println("|");
    }

    @Test
    public void testFindPathWithMaxWeightFromOrigin_CaseTwo() {
        GraphFindable findable = new SimpleGraphFinder();
        DirectedAcyclicGraph graph = new DirectedAcyclicGraph();
        DirectedAcyclicGraph.Node nodeA = new DirectedAcyclicGraph.Node("A", 1);
        DirectedAcyclicGraph.Node nodeB = new DirectedAcyclicGraph.Node("B", 2);
        DirectedAcyclicGraph.Node nodeC = new DirectedAcyclicGraph.Node("C", 2);
        DirectedAcyclicGraph.Node nodeD = new DirectedAcyclicGraph.Node("D", 4);
        DirectedAcyclicGraph.Node nodeE = new DirectedAcyclicGraph.Node("E", 3);

        graph.addEdge(nodeA, nodeB);
        graph.addEdge(nodeA, nodeC);
        graph.addEdge(nodeB, nodeC);
        graph.addEdge(nodeC, nodeD);
        graph.addEdge(nodeC, nodeE);
        graph.addEdge(nodeD, nodeA);
        graph.addEdge(nodeE, nodeD);

        LinkedList<DirectedAcyclicGraph.Node> nodes = findable.findPathWithMaxWeightFromOrigin(graph, nodeA);
        assertEquals(5, nodes.size());
        assertEquals("A", nodes.get(0).getName());
        assertEquals("B", nodes.get(1).getName());
        assertEquals("C", nodes.get(2).getName());
        assertEquals("E", nodes.get(3).getName());
        assertEquals("D", nodes.get(4).getName());
        nodes.forEach(node -> System.out.print(node.getName() + " -> "));
        System.out.println("|");
    }
}
