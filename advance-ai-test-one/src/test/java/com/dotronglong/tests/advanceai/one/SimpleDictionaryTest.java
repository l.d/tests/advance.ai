package com.dotronglong.tests.advanceai.one;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SimpleDictionaryTest {

    @Test
    public void testLoad() {
        DictionaryReadable readable = new SimpleDictionary();
        String text;
        List<Map<String, String>> dictionary;

        text = "";
        dictionary = readable.load(text);
        assertEquals(0, dictionary.size());

        text = "key1=value1;key2=value2\nkeyA=valueA\n";
        dictionary = readable.load(text);
        assertEquals(2, dictionary.size());

        text = "key1=value1;key2=value2\nkeyA=\n";
        dictionary = readable.load(text);
        assertEquals(2, dictionary.size());
        assertTrue(dictionary.get(1).get("keyA").equals(""));

        text = "key1=value1;key2=value2\nkeyA=valueA\nkey3=value1;key4=value2;key5=value2";
        dictionary = readable.load(text);
        assertEquals(3, dictionary.size());
        assertEquals(2, dictionary.get(0).size());
        assertEquals(1, dictionary.get(1).size());
        assertEquals(3, dictionary.get(2).size());
    }

    @Test
    public void testStore() {
        DictionaryWritable writable = new SimpleDictionary();
        String text;
        List<Map<String, String>> dictionary;

        dictionary = new ArrayList<>();
        text = writable.store(dictionary);
        assertTrue(text.equals(""));

        dictionary = new ArrayList<Map<String, String>>() {{
            add(new HashMap<String, String>() {{
                put("keyA", "valueA");
                put("keyB", "valueB");
            }});
            add(new HashMap<String, String>() {{
                put("keyC", "valueC");
                put("keyD", "valueD");
            }});
            add(new HashMap<String, String>() {{
                put("keyE", "valueE");
            }});
        }};
        text = writable.store(dictionary);
        assertEquals("keyA=valueA;keyB=valueB\nkeyC=valueC;keyD=valueD\nkeyE=valueE\n", text);
    }
}
