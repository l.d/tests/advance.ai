package com.dotronglong.tests.advanceai.one;

import java.util.List;
import java.util.Map;

public interface DictionaryWritable {
    String store(List<Map<String, String>> dictionary);
}
