package com.dotronglong.tests.advanceai.one;

import java.util.List;
import java.util.Map;

public interface DictionaryReadable {
    List<Map<String, String>> load(String text);
}
