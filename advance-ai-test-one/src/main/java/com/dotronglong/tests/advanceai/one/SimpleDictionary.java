package com.dotronglong.tests.advanceai.one;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SimpleDictionary implements DictionaryReadable, DictionaryWritable {

    public SimpleDictionary() {
    }

    public List<Map<String, String>> load(String text) {
        List<Map<String, String>> dictionary = new ArrayList<>();
        if (text == null || text.trim().isEmpty()) {
            return dictionary;
        }
        String[] lines = text.split("\n");
        for (String line : lines) {
            if (line.trim().isEmpty()) {
                continue;
            }

            Map<String, String> maps = new HashMap<>();
            Pattern pattern = Pattern.compile("([\\w-]+)+");
            Matcher matcher = pattern.matcher(line);
            while (matcher.find()) {
                maps.put(matcher.group(), matcher.find() ? matcher.group() : "");
            }
            dictionary.add(maps);
        }

        return dictionary;
    }

    public String store(List<Map<String, String>> dictionary) {
        StringBuffer buffer = new StringBuffer();
        for (Map<String, String> maps : dictionary) {
            maps.forEach((key, value) -> buffer.append(key).append("=").append(value).append(";"));
            buffer.deleteCharAt(buffer.length() - 1); // remove last semi-colon
            buffer.append("\n");
        }

        return buffer.toString();
    }
}
