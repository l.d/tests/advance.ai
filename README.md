# AVANCE.AI

### How To Test

**Prerequisites**:

- Java 8
- Maven

**Testing**:

```bash
# test first exercise 
cd advance-ai-test-one
mvn clean test

# test second exercise
cd ../advance-ai-test-two
mvn clean test
```